import java.util.Random;

/**
 * 
 */

/**
 *
 *
 */
public class Passenger {
	public int id;
	/**
	 * The weight of the passenger in kilograms.
	 */
	private int weight;
	/**
	 * The preference as to walking up or down the stairs.
	 */
	private Direction preferredDirection;
	/**
	 * If this runs out, the passenger takes the stairs instead.
	 */
	private int patienceLevel;
	/**
	 * The floor number of the destination.
	 */
	private int destination;
	/**
	 * The total number of passengers spawned so far.
	 */
	private static int total;
	private int currentFloor;
	private int queueNo;
	
	public Passenger() {
		Random random = new Random();
		int maxWeight = 80;
		int minWeight = 20;
		id = total;
		total++;
		preferredDirection = Direction.getRandom();
		weight = random.nextInt((maxWeight - minWeight) + 1) + minWeight;
		patienceLevel = 50;
	}
	
	/**
	 * Similar to how a passenger pushes the button by the elevator.
	 * 
	 * @param aFloor Represents a floor level.
	 */
	public void callElevator(Building aBuilding, int aFloorNo, Direction aDirection) {
		StopRequest stopRequest = new StopRequest(aFloorNo, aDirection);
		aBuilding.elevators.get(queueNo).stopRequests.set(aFloorNo-1, stopRequest);
	}
	
	/**
	 * Displays the Passenger object.
	 */
	public void display() {
		System.out.print("[" + id + "]");
	}
	
	public int weight() {
		return weight;
	}
	
	public int getDestination() {
		return destination;
	}
	
	public boolean wantsDown() {
		if (preferredDirection == Direction.DOWN)
			return true;
		else
			return false;
	}
}
