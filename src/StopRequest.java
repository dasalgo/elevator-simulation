/**
 * 
 */

/**
 * @author Gian Carlo Mina
 *
 */
public class StopRequest {
	public boolean isActive;
	public int floorNo;
	public Direction requestedDirection;
	
	public StopRequest() {
		
	}
	
	public StopRequest(int aFloorNo, Direction aDirection) {
		floorNo = aFloorNo;
		requestedDirection = aDirection;
		isActive = true;
	}
}
