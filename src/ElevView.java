import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;


@SuppressWarnings("serial")
public class ElevView extends JFrame {

	public ElevView() {
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 200, 400);
				//this.getPreferredSize().height);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(new FlowLayout(FlowLayout.CENTER, 5, 5));
		SimulationView[] simulation = new SimulationView[Building.noOfElevators];
		for (int j = 0; j < Building.noOfElevators; j++) {
			simulation[j] = new SimulationView(Building.elevators.get(j));
		}
		
		JButton[] elevatorButton = new JButton[Building.noOfElevators];
		for (int i = 0; i < Building.noOfElevators; i++) {
			elevatorButton[i] = new JButton("Elevator " + Building.elevators.get(i).id);
			SimulationView currentSimulation = simulation[i]; 
			elevatorButton[i].addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					currentSimulation.enableWindow(true);
				}
			});
			this.add(elevatorButton[i]);
		}
	}
	
	public void enableWindow(boolean status)
	{
		this.setVisible(status);
		this.setEnabled(status);
		if (status == false)
			this.dispose();
	}

	private JPanel contentPane;
}
