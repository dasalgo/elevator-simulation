
public enum Direction {
	UP,
	DOWN;
	
	public static Direction getRandom() {
        return values()[(int) (Math.random() * values().length)];
    }
}
