import javax.swing.SwingUtilities;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;

/**
 * 
 */

/**
 *
 */
public class Main {

	public static void main(String[] args) {
		Building building;
		Passenger test;

		MainMenuView mainMenu = new MainMenuView();
		try {
            UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (UnsupportedLookAndFeelException e) {
            e.printStackTrace();
        }

        SwingUtilities.updateComponentTreeUI(mainMenu);
        mainMenu.enableWindow(true);
		
		/*
		
		int selectedFloor = 1;

		building = new Building(4, 4);
		test = new Passenger();

		building.accessFloor(1, 1, test);

		building.accessFloor(1, 1, new Passenger());
		building.accessFloor(1, 1, new Passenger());
		building.accessFloor(1, 1, new Passenger());
		building.accessFloor(1, 1, new Passenger());

		building.accessFloor(1, 2, new Passenger());
		building.accessFloor(1, 2, new Passenger());
		building.accessFloor(1, 3, new Passenger());
		building.accessFloor(1, 3, new Passenger());
		building.accessFloor(1, 4, new Passenger());
		
		test.callElevator(building, 1, Direction.UP);
		
		test.callElevator(building, 3, Direction.DOWN);
		
		building.displayFloor(1);
		
		while (true) {
			for (Elevator e : Building.elevators) {
				e.launch();
			}
			building.displayFloor(1);
		}

		//building.displayElevators();
		*/
		building = new Building(4, 4);
        
        building.spawninQueue(true, 30);
	}

}
