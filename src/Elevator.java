import java.util.ArrayList;

public class Elevator implements Runnable {

	/**
	 * A number that is unique to the elevator.
	 */
	public int id;

	/**
	 * The maximum weight that the elevator can accomodate.
	 */
	private int maxCapacity;

	/**
	 * The list of floors that the elevator is stopping by.
	 */
	public ArrayList<StopRequest> stopRequests;

	/**
	 * The time(in seconds) that the elevator takes to move from one floor to
	 * another.
	 */
	private int transitionTime;

	/**
	 * Tells if the elevator is currently on high-speed mode.
	 */
	private boolean isHighSpeed;

	/**
	 * The list of floors that requested the elevator to stop by.
	 */
	// private CallStack stopRequests;

	/**
	 * The direction of the elevator's movement.
	 */
	private Direction elevatorDirection;

	/**
	 * The floor the elevator is currently on.
	 */
	public Floor currentFloor;

	/**
	 * The maximum floor that the elevator can reach.
	 */
	private int maxFloor;

	/**
	 * Tells whether the elevator's door is open or not.
	 */
	private boolean isOpen;

	/**
	 * The current weight of all passengers inside the elevator
	 */
	private int carriedWeight;

	/**
	 * All the passengers inside the elevator
	 */
	private ArrayList<Passenger> carriedPassengers;

	/**
	 * The list of floors that the passengers have chosen to get off on
	 */
	private ArrayList<Integer> destinations;

	/**
	 * The list of floors that the elevator is allowed to stop at
	 */
	private ArrayList<Integer> floorStops;

	/**
	 * The current direction of the elevator
	 */
	private Direction currentDirection;

	/**
	 * The next floor
	 */
	private int nextFloor;

	public Elevator() {
		id = Building.noOfElevators;
		Building.noOfElevators++;
		currentFloor = Building.floors.get(0);
		stopRequests = new ArrayList<StopRequest>();
		for (int i = 0; i < Building.noOfFloors; i++) {
			stopRequests.add(new StopRequest());
			stopRequests.get(i).floorNo = i + 1;
		}
		carriedPassengers = new ArrayList<Passenger>();
		destinations = new ArrayList<Integer>();
		carriedWeight = 0;
		currentDirection = Direction.UP;
	}

	/**
	 * Initializes the Elevator with a specified maximum floor.
	 * 
	 * @param aMaxFloor
	 *            The maximum floor that the elevator can reach.
	 */
	public Elevator(int aMaxFloor) {
		id = Building.noOfElevators;
		Building.noOfElevators++;
		currentFloor = Building.floors.get(0);
		maxFloor = aMaxFloor;
		stopRequests = new ArrayList<StopRequest>();
		destinations = new ArrayList<Integer>();
		floorStops = new ArrayList<Integer>();
		for (int i = 0; i < Building.noOfFloors; i++) {
			stopRequests.add(new StopRequest());
		}
	}

	// public void update

	public void updateNextFloor() {
		if (currentDirection == Direction.UP) {
			for (int i : destinations) {
				if (i >= currentFloor.floorNo) {
					nextFloor = i;
					break;
				}
			}
		}
	}
	
	public void moveToNextFloor() {
		currentFloor = Building.floors.get(Building.floors.indexOf(nextFloor));
	}

	/**
	 * Moves the elevator one floor up;
	 */
	public void ascend() {
		if (currentFloor.floorNo < maxFloor) {
			currentFloor = Building.floors.get(currentFloor.floorNo);
			
		}
	}

	/**
	 * Moves the elevator one floor down;
	 */
	public void descend() {
		if (currentFloor.floorNo > 0) {
			currentFloor = Building.floors.get(currentFloor.floorNo - 2);
		}
	}

	public void addPassenger(PassengerQueue aQueue) {
		if (carriedWeight + aQueue.head().weight() <= maxCapacity) {
			carriedPassengers.add(aQueue.dequeue());
		}
	}


	public void displayStopRequests() {
		System.out.println("Elevator " + id);
		for (int i = stopRequests.size() - 1; i >= 0; i--) {
			if (stopRequests.get(i).isActive)
				System.out.println("Request from 		floor " + (stopRequests.get(i).floorNo) + ": Go "
						+ stopRequests.get(i).requestedDirection);
			else
				System.out.println("No request from 	floor " + (stopRequests.get(i).floorNo));
		}
	}

	public void addDestination(int floorNo) {
		// destinations.add(floorNo);
		addSortArrayList(destinations, floorNo);
	}

	public void addSortArrayList(ArrayList<Integer> a, int element) {
		// ArrayList<Integer> a = new ArrayList<Integer> (0);
		int i, j, k, tempVar;

		// for (k = 100; k > 0; k -= 10) {
		// System.out.println("Program is adding: " + k);
		a.add(element);

		for (i = 0; i < a.size(); i++) {
			for (j = 0; j < a.size(); j++) {
				// System.out.println("Comparing index " + j + ": " + a.get(j) +
				// " with index " + (j + 1) + ": " + a.get(j + 1));
				if (j + 1 < a.size()) {
					if (a.get(j) > a.get(j + 1)) {
						tempVar = a.get(j);
						a.set(j, a.get(j + 1));
						a.set(j + 1, tempVar);
					}
				}
			}
		}
		/*
		 * System.out.println("Arranged set:"); System.out.print("["); for (i =
		 * 0; i < a.size(); i++) { System.out.print(" " + a.get(i)); if (i <
		 * a.size() - 1) System.out.print(","); } System.out.println(" ]");
		 */
		// System.out.println(a);
		// }
	}

	/**
	 * Displays the elevator object.
	 */
	public void display() {
		if (isOpen) {
			System.out.print("[   ] ");
		} else {
			System.out.print("[|||] ");
		}
	}

	/**
	 * Sets the maximum capacity of the elevator object
	 */
	public void setMaxCapacity(int capacity) {
		maxCapacity = capacity;
	}

	/**
	 * Sets the transition time
	 */
	public void setTransitionTime(int seconds) {
		transitionTime = seconds;
	}

	/**
	 * Gets the maximum capacity of the elevator object
	 * 
	 * @return maximum weight capacity of elevator
	 */
	public int getMaxCapacity() {
		return maxCapacity;
	}

	/**
	 * Gets the transition time
	 * 
	 * @return elevator transition time
	 */
	public int getTransitionTime() {
		return transitionTime;
	}

	public void setFloorStops(ArrayList<Integer> newStops) {
		floorStops = newStops;
	}
	
	public int getCarriedWeight()
	{
		return carriedWeight;
	}
	
	public ArrayList<Integer> getFloorStops()
	{
		return floorStops;
	}
	
	@Override
	public String toString() {
		return "Elevator # " + id;
	}

	public void dropPassengers() {
		for (Passenger p : carriedPassengers) {
			if (p.getDestination() == currentFloor.floorNo) {
				carriedPassengers.remove(p);
				destinations.remove(currentFloor.floorNo);
			}
		}
	}

	public void loadPassengers(PassengerQueue aQueue) {
		Passenger nextInLine;
		while (aQueue.head().weight() + carriedWeight <= maxCapacity) {
			nextInLine = aQueue.dequeue();
			carriedPassengers.add(nextInLine);
			addSorted(destinations, closestFloor(nextInLine));
		}
	}
	
	public void addSorted(ArrayList<Integer> a, int element) {
		int i, j, k, tempVar;

			//System.out.println("Program is adding: " + k);
			a.add(element);
			
			for (i = 0; i < a.size(); i++) {
				for (j = 0; j < a.size(); j++) {
					// System.out.println("Comparing index " + j + ": " + a.get(j) + " with index " + (j + 1) + ": " + a.get(j + 1));
					if (j + 1 < a.size()) {
						if (a.get(j) > a.get(j + 1)) {
							tempVar = a.get(j);
							a.set(j, a.get(j + 1));
							a.set(j + 1, tempVar);
						}
					}
				}
		/*
			System.out.println("Arranged set:");
			System.out.print("[");
			for (i = 0; i < a.size(); i++) {
				System.out.print(" " + a.get(i));
				if (i < a.size() - 1)
					System.out.print(",");
			}
			System.out.println(" ]");
		*/
			System.out.println(a);
		}
	}

	public int closestFloor(Passenger aPassenger) {
		/*
		 * supposed function must contain: 1. ELEV - ArrayList<Integer> stops
		 * arraylist of elevator stops 2. ELEV - int maxElev OPTIONAL VARIABLE
		 * -> see notes below; is the topmost floor/stop available for the
		 * elevator UPDATE: will no longer be using this variable 3. PERS - int
		 * destFloor person's destination 4. PERS - boolean wantsDown if TRUE,
		 * person wants to go DOWN else, person wants to go UP
		 */
		int destFloor = aPassenger.getDestination(); // destination of the
														// PASSENGER
		boolean wantsDown = aPassenger.wantsDown(); // preference (if TRUE -> down, if FALSE -> up)
		// int elevMax; // max destination for elevator
		ArrayList<Integer> stops = new ArrayList<Integer>(0); // elevator stops
		int getOffAt = 1; // the floor available to user based on dest. and
							// pref.

		int i;
		// int j, input, tempVar;
		// boolean boundaryIsAStop = false; // in ELEV, self-explaining
		// boolean oneOfStops = false; // in ELEV, if input stop was already
		// previously entered
		boolean destIsAStop = false; // in PERS, self-explaining
		// Scanner sc = new Scanner(System.in);
		if (destFloor > stops.get(stops.size() - 1)) // if "destFloor" is above
														// "elevMax"
			// if no "elevMax" will be passed, can be changed to
			// "stops.get(stops.size() - 1)"
			getOffAt = stops.get(stops.size() - 1);
		// if no "elevMax" will be passed, can be changed to
		// "stops.get(stops.size() - 1)"
		else if (destFloor == 1) // if already at ground
			System.out.println("Person already at ground floor!");
		else if (destFloor < stops.get(0) && !wantsDown) { // handles the
															// situation if
															// person wants to
															// go up but lowest
															// stop
			// besides ground is above the person's destination
			// (which will cause the person TO GO DOWN if he got off at the
			// lowest stop)
			// (BUT HE WANTS TO GO UP)
			// System.out.println("lol skrub");
			System.out.println("Person didn't ride at all.");
			getOffAt = 1;
		} else {
			for (i = 0; i < stops.size(); i++) {
				if (stops.get(i) == destFloor) {
					getOffAt = stops.get(i);
					destIsAStop = true;
				}
			}

			if (!destIsAStop) {
				if (wantsDown) {
					i = 0;
					do {
						if (stops.get(i) > destFloor)
							getOffAt = stops.get(i);
						i++;
					} while (stops.get(i - 1) < destFloor);
				} else { // wants to go up
					i = 0;
					do {
						if (stops.get(i) < destFloor)
							getOffAt = stops.get(i);
						i++;
					} while (stops.get(i - 1) < destFloor);
				}
			}
		}
		/*
		 * UNNECESSARY PRINTS System.out.println("Elevator Stops: " + stops);
		 * System.out.print("Person who wants to go to " + destFloor +
		 * " with a preference of going "); if (wantsDown)
		 * System.out.println("down"); else System.out.println("up"); if
		 * (getOffAt > 1) System.out.println("got off at " + getOffAt); else
		 * System.out.println("did not ride at all.");
		 */
		// more or less where you put "return getOffAt;"
		// END OF ALGO
		// }
		return getOffAt;
	}

	
	public void launch() {
		loadPassengers(Building.floors.get(id).getQueue(id));
		updateNextFloor();
		try {
			Thread.sleep(transitionTime * 1000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		moveToNextFloor();
	}
	
	@Override
	public void run() {
		launch();
	}
}
