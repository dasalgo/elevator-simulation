import java.util.ArrayList;

/**
 * Represents a floor level.
 */
public class Floor {
	/**
	 * The floor number. e.g. (floorNo = 4) -> 4th floor.
	 */
	public final int floorNo;
	/**
	 * The total number of passengers currently on the floor.
	 */
	private int noOfPassengers;
	/**
	 * The lines of passengers to a specific elevator.
	 */
	private ArrayList<PassengerQueue> lines;
	// private final int QUEUE_LIMIT;

	/**
	 * Initializes the Floor with a specified floor number.
	 * 
	 * @param aFloorNo
	 *            The floor number. e.g. (floorNo = 4) -> 4th floor.
	 */
	public Floor(int aFloorNo) {
		// QUEUE_LIMIT = 10;
		floorNo = aFloorNo;
		lines = new ArrayList<PassengerQueue>();
		for (int i = 0; i < Building.noOfElevators; i++) {
			lines.add(new PassengerQueue());
		}
	}

	/**
	 * Requests the elevator to stop by on this floor. i.e. Pressing the buttons
	 * by the elevator.
	 * 
	 * @param aDirection
	 *            The direction whether UP or DOWN.
	 */
	public void callElevator(Direction aDirection) {

	}

	/**
	 * @param aLineNo
	 *            The number that identifies one of the queues for the
	 *            elevators.
	 * @param aPassenger
	 *            A passenger that is lining up for the elevator.
	 */
	public void addToQueue(int aLineNo, Passenger aPassenger) {
		lines.get(aLineNo - 1).enqueue(aPassenger);
	}

	/**
	 * Displays te contents of the Floor.
	 */
	public void display() {
		// for (Elevator e : Building.elevators) {
		// e.display();
		// System.out.print("|");
		//// System.out.print(lines.get(e.id).size());
		// lines.get(e.id-4).displayQueue();
		// System.out.print("|");
		// System.out.println();
		// }
		for (int i = 0; i < Building.elevators.size(); i++) {
			Building.elevators.get(i).display();
			System.out.print("|");
			lines.get(i).displayQueue();
			System.out.print("|");
			System.out.println();
		}
		// for (PassengerQueue pq : lines) {
		// System.out.print("|");
		// pq.displayQueue();
		// System.out.print("|");
		// System.out.println();
		// }
	}
	
	public PassengerQueue getQueue(int num) {
		return lines.get(num);
	}
}
