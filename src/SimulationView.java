import java.awt.Graphics;
import java.util.ArrayList;
import javax.swing.ImageIcon;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextPane;

import java.awt.Color;

import javax.swing.JPanel;
@SuppressWarnings("serial")

public class SimulationView extends JFrame {
	
	public SimulationView(Elevator currentElev) {
		activeElev = currentElev;
		this.setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
		this.setTitle("Elevator #" + currentElev.id);
		this.setBounds(0, 0, 800, 600);
		getContentPane().setLayout(null);
		
		JPanel panel_1 = new JPanel();
		panel_1.setBounds(0, 0, 784, 561);
		panel_1.setLayout(null);
		getContentPane().add(panel_1);
		panel_1.setOpaque(false);
		
		textPane = new JTextPane();
		textPane.setBounds(285, 417, 489, 133);
		panel_1.add(textPane);
		
		JPanel panel = new JPanel();
		panel.setBounds(0, 0, 784, 561);
		getContentPane().add(panel);
		panel.setLayout(null);
		
		JLabel lblNewLabel = new JLabel("");
		lblNewLabel.setBounds(0, 0, 817, 581);
		lblNewLabel.setIcon(new ImageIcon(this.getClass().getResource("/elev_bg.jpg")));
		panel.add(lblNewLabel);
		
		column += 30;
	}
	
	public void enableWindow(boolean status)
	{
		this.setVisible(status);
		this.setEnabled(status);
		if (status == false)
			this.dispose();
	}
	
	public void setTextPane(int currentLoad, int maxWeightCapacity, 
			ArrayList<Integer> levels, int timeToMove, int currentFloor)
	{
		currentLoad = activeElev.getCarriedWeight();
		maxWeightCapacity = activeElev.getMaxCapacity();
		levels = activeElev.getFloorStops();
		timeToMove = activeElev.getTransitionTime();
		currentFloor = activeElev.currentFloor.floorNo;
		
		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < levels.size(); i++) {
			sb.append(levels.get(i));
			if (i + 1 != levels.size())
				sb.append(", ");
		}
		
		textPane.setText("Current Load: " + currentLoad +
				"\nMax Weight Capacity: " + maxWeightCapacity +
				"\nLevels: " + sb + 
				"\nTime to Move: " + timeToMove +
				"\nCurrent Floor: " + currentFloor);
		
		this.validate();
		this.repaint();
	}
	
	class SpawnedPassenger extends JComponent 
	{
	    @Override
	    public void paintComponent(Graphics g) {
	    	super.paintComponents(g);
			g.setColor(Color.BLACK);
		    g.fillOval(220, column += 10, 5, 5);
	    }
	}
	
	public void spawnPassenger() 
	{
		this.getContentPane().add(new SpawnedPassenger());
	}
	
	private JTextPane textPane;
	private Elevator activeElev;
	private int column = 0;
}
