import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JButton;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JLabel;

@SuppressWarnings("serial")
public class MainMenuView extends JFrame {

	public MainMenuView() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setTitle("Elevator Simulator ver. 1.0");
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		/* Start Simulation Button */
		JButton btnStartSimulation = new JButton("Start Simulation");
		btnStartSimulation.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				ElevView elevatorList = new ElevView();
				elevatorList.enableWindow(true);
//				SimulationView sim = new SimulationView();
//				sim.enableWindow(true);
			}
		});
		btnStartSimulation.setFont(new Font("Tahoma", Font.PLAIN, 17));
		btnStartSimulation.setBounds(79, 111, 266, 64);
		contentPane.add(btnStartSimulation);
		
		/* Settings Button */
		JButton btnSettings = new JButton("Settings");
		btnSettings.setFont(new Font("Tahoma", Font.PLAIN, 17));
		btnSettings.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				SettingsView settings = new SettingsView();
				settings.enableWindow(true);
			}
		});
		btnSettings.setBounds(79, 186, 266, 64);
		contentPane.add(btnSettings);
		
		JLabel lblElevatorSimulator = new JLabel("Elevator Simulator");
		lblElevatorSimulator.setFont(new Font("Tahoma", Font.PLAIN, 40));
		lblElevatorSimulator.setBounds(55, 26, 321, 48);
		contentPane.add(lblElevatorSimulator);
		
		JLabel lblVersion = new JLabel("Version 1.0");
		lblVersion.setBounds(323, 71, 54, 21);
		contentPane.add(lblVersion);
	}
	
	public void enableWindow(boolean status)
	{
		this.setVisible(status);
		this.setEnabled(status);
		if (status == false)
			this.dispose();
	}
	
	private JPanel contentPane;
}
