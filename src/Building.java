import java.util.ArrayList;

/**
 * Represents a building.
 */
public class Building {
	/**
	 * The number of elevators in the building.
	 */
	static int noOfElevators;
	/**
	 * The list of elevators in the building.
	 */
	static ArrayList<Elevator> elevators;
	/**
	 * The number of floors that COMPOSE the building.
	 */
	static int noOfFloors;
	/**
	 * The list of floors that COMPOSE the building.
	 */
	static ArrayList<Floor> floors;
	/**
	 * The rate of spawning of passengers.
	 */
	static int spawnRate;
	private PassengerSpawner pSpawn;

	public Building(int aNoOfElevators, int aNoOfFloors) {
		//noOfElevators = aNoOfElevators;
		noOfFloors = aNoOfFloors;

		elevators = new ArrayList<Elevator>(aNoOfElevators);
		floors = new ArrayList<Floor>(noOfFloors);
		
		pSpawn = new PassengerSpawner();


		for (int i = 0; i < aNoOfFloors; i++) {
			floors.add(new Floor(i + 1));
		}
		
		for (int i = 0; i < aNoOfElevators; i++) {
			elevators.add(new Elevator(noOfFloors));
		}
		
		
	}

	/**
	 * This method accesses the specified floor and adds a Passenger to the
	 * specified queue.
	 * 
	 * @param aFloorNo
	 *            The floor number. e.g. (floorNo = 4) -> 4th floor.
	 * @param aLineNo
	 *            The number that identifies one of the queues for the
	 *            elevators.
	 * @param aPassenger
	 *            A passenger that is lining up for the elevator.
	 */
	public void accessFloor(int aFloorNo, int aLineNo, Passenger aPassenger) {
		System.out.println(aFloorNo - 1);
		floors.get(aFloorNo - 1).addToQueue(aLineNo, aPassenger);
		elevators.get(aLineNo).addDestination(aFloorNo);
	}

	/**
	 * Displays the contents of the specified floor.
	 * 
	 * @param aFloorNo The floor number. e.g. (floorNo = 4) -> 4th floor.
	 */
	public void displayFloor(int aFloorNo) {
		floors.get(aFloorNo - 1).display();
	}
	
	public void displayElevators() {
		for (int i = noOfFloors-1; i >= 0; i--) {
			for (Elevator e : elevators) {
				if (e.currentFloor.equals(floors.get(i))) {
					e.display();
				}
				else
					System.out.print("  |   ");
			}
			System.out.println();
		}
	}
	
	public void spawninQueue(boolean isPlaying, int rate)
	{Passenger a;
	Double d;
	d = Math.random();
		//gLines = floors.get(0).lines;
		while(isPlaying == true)
		{
			if(d < (double)(rate / 1000.0))
			{
				for(int i = 0; i < Building.noOfElevators; i++){
					a = pSpawn.beginSpawning();
					floors.get(0).getQueue(i).enqueue(a);
				}
			}
		}

	}
}
