
/**
 * Represents one of the queues for the elevators.
 */
public class PassengerQueue extends Queue<Passenger> {
//	private final int QUEUE_MAX;
	
	/**
	 * Displays the contents of the queue.
	 */
	public void displayQueue() {
		int originalSize = size();
		Passenger selected;
		
		for (int i = 0; i < originalSize; i++) {
			selected = dequeue();
			selected.display();
			enqueue(selected);
		}
	}
}
