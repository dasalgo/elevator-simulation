import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JComboBox;
import javax.swing.JSpinner;
import javax.swing.JTextField;
import javax.swing.SpinnerNumberModel;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.awt.event.ActionEvent;

@SuppressWarnings("serial")
public class ElevatorSettingsView extends JFrame {

	public ElevatorSettingsView() {
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 400, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		setTitle("Edit Elevator Settings");
		
		/* Elevator */
		JLabel elevatorLabel = new JLabel("Elevator");
		elevatorLabel.setBounds(29, 39, 46, 14);
		contentPane.add(elevatorLabel);
		
		JComboBox<Elevator> elevatorDropDown = new JComboBox<Elevator>();
		for (int i = 0; i < Building.elevators.size(); i++) {
			elevatorDropDown.addItem(Building.elevators.get(i));
		}
		elevatorDropDown.setBounds(85, 36, 273, 20);
		contentPane.add(elevatorDropDown);
		
		/* Max Weight */
		JLabel maxWeightLabel = new JLabel("Maximum Weight Capacity");
		maxWeightLabel.setBounds(29, 77, 134, 14);
		contentPane.add(maxWeightLabel);
		
		JSpinner maxWeight = new JSpinner();
		maxWeight.setModel(new SpinnerNumberModel(1200, 20, Integer.MAX_VALUE, 1));
		maxWeight.setBounds(175, 74, 183, 20);
		contentPane.add(maxWeight);
		
		/* Elev Stops */
		JLabel elevStopsLabel = new JLabel("Stops");
		elevStopsLabel.setBounds(29, 117, 46, 14);
		contentPane.add(elevStopsLabel);
		
		JTextField elevStops = new JTextField();
		elevStops.setBounds(66, 114, 292, 20);
		contentPane.add(elevStops);
		elevStops.setColumns(10);
		
		/* Separate Stops with Comma */
		JLabel separateStops = new JLabel("*Separate stops with comma");
		separateStops.setBounds(66, 138, 157, 14);
		contentPane.add(separateStops);
		
		/* Transition Time */
		JLabel transitionTimeLabel = new JLabel("Transition time");
		transitionTimeLabel.setBounds(29, 163, 70, 14);
		contentPane.add(transitionTimeLabel);
		
		JSpinner transitionTime = new JSpinner();
		transitionTime.setModel(new SpinnerNumberModel(5, 1, 10, 1));
		transitionTime.setBounds(109, 163, 193, 20);
		contentPane.add(transitionTime);
		
		JLabel secondsLabel = new JLabel("seconds");
		secondsLabel.setBounds(312, 163, 46, 14);
		contentPane.add(secondsLabel);
		
		JButton btnSave = new JButton("Save");
		btnSave.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				/* Get Elevator First */
				Elevator activeElev = (Elevator)(elevatorDropDown.getSelectedItem());
				/* Get Spinner Value, set as Elevator Max Weight */
				activeElev.setMaxCapacity((Integer)(maxWeight.getValue()));
				/* Get Text Field Contents, split Strings into array, add to ArrayList */
				String[] stops = elevStops.getText().split(",");
				ArrayList<Integer> stopsArrayList = new ArrayList<Integer>();
				for (int i = 0; i < stops.length; i++) {
					stopsArrayList.add(Integer.parseInt(stops[i]));
				}
				/* Set stops in elevator */
				activeElev.setFloorStops(stopsArrayList);
				/* Set elev transition */
				activeElev.setTransitionTime((Integer)(transitionTime.getValue()));
			}
		});
		btnSave.setBounds(175, 212, 89, 23);
		contentPane.add(btnSave);
		
		JButton btnClose = new JButton("Close");
		btnClose.addActionListener(new ActionListener () {
			public void actionPerformed(ActionEvent e) {
				enableWindow(false);
			}
		});
		btnClose.setBounds(269, 212, 89, 23);
		contentPane.add(btnClose);
	}
	
	public void enableWindow(boolean status)
	{
		this.setVisible(status);
		this.setEnabled(status);
		if (status == false)
			this.dispose();
	}
	
	private JPanel contentPane;
}
