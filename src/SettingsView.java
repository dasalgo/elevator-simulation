import javax.swing.JFrame;
import javax.swing.JSpinner;
import javax.swing.SpinnerNumberModel;
import javax.swing.JLabel;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;

@SuppressWarnings("serial")
public class SettingsView extends JFrame {

	public SettingsView() {
		setBounds(100, 100, 450, 360);
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setTitle("Elevator Simulator ver 1.0 : General Settings");
		getContentPane().setLayout(null);
		
		JLabel lblBuildingSettings = new JLabel("Building Settings");
		lblBuildingSettings.setBounds(153, 20, 116, 17);
		lblBuildingSettings.setFont(new Font("Tahoma", Font.BOLD, 14));
		getContentPane().add(lblBuildingSettings);
		
		/* Number of Floors */
		JLabel lblNumberOfFloors = new JLabel("Number of Floors");
		lblNumberOfFloors.setBounds(89, 48, 82, 14);
		getContentPane().add(lblNumberOfFloors);
		
		floorCount = new JSpinner();
		floorCount.setModel(new SpinnerNumberModel(5, 1, 200, 1));
		floorCount.setBounds(279, 45, 55, 20);
		getContentPane().add(floorCount);
		
		/* Number of Elevators */
		JLabel lblNumberOfElevators = new JLabel("Number of Elevators");
		lblNumberOfElevators.setBounds(89, 74, 98, 14);
		getContentPane().add(lblNumberOfElevators);
		
		elevCount = new JSpinner();
		elevCount.setBounds(279, 71, 55, 20);
		elevCount.setModel(new SpinnerNumberModel(1, 1, 20, 1));
		getContentPane().add(elevCount);
		
		JLabel lblNewLabel = new JLabel("Elevator Settings");
		lblNewLabel.setBounds(152, 122, 118, 17);
		lblNewLabel.setFont(new Font("Tahoma", Font.BOLD, 14));
		getContentPane().add(lblNewLabel);
		
		/* Edit Elevator Setttings */
		editElevs = new JButton("Edit Settings Per Elevator");
		editElevs.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				ElevatorSettingsView elevSettings = new ElevatorSettingsView();
				elevSettings.enableWindow(true);
			}
		});
		editElevs.setBounds(89, 145, 245, 23);
		getContentPane().add(editElevs);
		
		JLabel lblPassengerSettings = new JLabel("Passenger Settings");
		lblPassengerSettings.setBounds(145, 199, 132, 17);
		lblPassengerSettings.setFont(new Font("Tahoma", Font.BOLD, 14));
		getContentPane().add(lblPassengerSettings);
		
		JLabel lblSpawningRate = new JLabel("Passenger Spawn percentage");
		lblSpawningRate.setBounds(89, 225, 216, 14);
		getContentPane().add(lblSpawningRate);
		
		spawnRate = new JSpinner();
		spawnRate.setModel(new SpinnerNumberModel(1, 1, 100, 1));
		spawnRate.setBounds(279, 222, 55, 20);
		getContentPane().add(spawnRate);
		
		btnSave = new JButton("Save");
		btnSave.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Building.noOfFloors = (Integer)(floorCount.getValue());
				Building.noOfElevators = (Integer)(elevCount.getValue());
				Building.spawnRate = (Integer)(spawnRate.getValue());
			}
		});
		btnSave.setBounds(89, 273, 121, 23);
		getContentPane().add(btnSave);
		
		btnClose = new JButton("Close");
		btnClose.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				enableWindow(false);
			}
		});
		btnClose.setBounds(210, 273, 124, 23);
		getContentPane().add(btnClose);
	}
	
	public void enableWindow(boolean status) 
	{
		this.setVisible(status);
		this.setEnabled(status);
		if (status == false)
			this.dispose();
	}
	
	private JSpinner spawnRate, elevCount, floorCount;
	
	private JButton btnSave, btnClose, editElevs;
}
